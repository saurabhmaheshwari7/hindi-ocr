This software takes an image containing hindi text either in Handwritten format or printed format, and converts it into Machine-Readable UTF-8 format using image processing and Neural Networks to train a pre-processed data set of Hindi Characters and Words and uses its results to recognize the user input.

How to use - Clone the repo and build using NetBeans IDE.