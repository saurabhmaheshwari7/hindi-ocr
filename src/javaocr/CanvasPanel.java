package javaocr;

import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.File;
import javax.swing.JLayeredPane;
import javax.swing.JPanel;

/**
 *
 * @author Saurabh Maheshwari
 */
public class CanvasPanel extends JPanel implements ComponentListener, MouseListener {

    public final MainWindow mainWindow;

    private ImageCanvas imageCanvas;
    private JLayeredPane layeredPane;

    public CanvasPanel(MainWindow mainWindow) {
        this.mainWindow = mainWindow;

        setLayout(new GridLayout(1, 2));
        
        layeredPane = new JLayeredPane();

        add(layeredPane);


        imageCanvas = new ImageCanvas(this);
        imageCanvas.setBackground((Color) Settings.get(Settings.IMAGE_CANVAS_BACKGROUND_COLOR));
        
        layeredPane.add(imageCanvas);
        layeredPane.setBackground(imageCanvas.getBackground());

        addComponentListener(this);
        imageCanvas.addMouseListener(this);
    }

    
    public void setImage(File file) {
        imageCanvas.setImage(file);
    }

    public File getImage() {
        return imageCanvas.getImageFile();
    }

    public boolean hasImage() {
        return imageCanvas.getImageFile() != null;
    }

    @Override
    public void componentResized(ComponentEvent e) {
        imageCanvas.setBounds(0, 0, layeredPane.getWidth(), layeredPane.getHeight());
        layeredPane.revalidate();
        imageCanvas.repaint();
    }

    @Override
    public void componentMoved(ComponentEvent e) {
    }

    @Override
    public void componentShown(ComponentEvent e) {
    }

    @Override
    public void componentHidden(ComponentEvent e) {
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        mainWindow.getMMenuBar().openImage();
    }

    @Override
    public void mousePressed(MouseEvent e) {
    }

    @Override
    public void mouseReleased(MouseEvent e) {
    }

    @Override
    public void mouseEntered(MouseEvent e) {
    }

    @Override
    public void mouseExited(MouseEvent e) {
    }
}
