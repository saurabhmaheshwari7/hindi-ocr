package javaocr;

import java.awt.EventQueue;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

/**
 *
 * @author Saurabh Maheshwari
 */
public class JavaOCR {

    public JavaOCR() {

    }

    public static void runUI() {
        EventQueue.invokeLater(() -> {
            try {
                UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
            } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException ignore) {
                try {
                    UIManager.setLookAndFeel("com.sun.java.swing.plaf.nimbus.NimbusLookAndFeel");
                } catch (Exception ex) {

                }
            }
            MainWindow mainWindow = new MainWindow();
            mainWindow.setVisible(true);
        });
    }

    public static void main(String args[]) {
//        File imageFile = new File("image_1.jpg");
//        ITesseract instance = new Tesseract();
//        instance.setLanguage("hin");
//        
//        try {
//            String result = instance.doOCR(imageFile);
//            System.out.println(result);
//        } catch (TesseractException e) {
//            System.out.println(e.getMessage());
//        }
        runUI();
    }
}
