package javaocr;

import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import javax.swing.JPopupMenu;
import javax.swing.JTextArea;
import javax.swing.KeyStroke;

/**
 *
 * @author Saurabh Maheshwari
 */
public class JTextAreaPopupMenu extends JPopupMenu {

    private final JTextArea textArea;

    public JTextAreaPopupMenu(JTextArea textArea) {
        this.textArea = textArea;

        this.add(new AbstractAction("Select All") {
            
            @Override
            public void actionPerformed(ActionEvent e) {
                textArea.selectAll();
            }

        });
        this.add(new AbstractAction("Copy") {

            @Override
            public void actionPerformed(ActionEvent e) {
                textArea.copy();
            }

        });
        this.add(new AbstractAction("Cut") {

            @Override
            public void actionPerformed(ActionEvent e) {
                textArea.cut();
            }

        });
        this.add(new AbstractAction("Paste") {

            @Override
            public void actionPerformed(ActionEvent e) {
                textArea.paste();
            }

        });
        this.add(new AbstractAction("Clear Text") {

            @Override
            public void actionPerformed(ActionEvent e) {
                textArea.setText("");
            }

        });
    }
}
