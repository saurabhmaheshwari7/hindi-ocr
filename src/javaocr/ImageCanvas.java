package javaocr;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.event.ActionEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.io.File;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

/**
 *
 * @author Saurabh Maheshwari
 */
public class ImageCanvas extends JPanel {

    private final CanvasPanel canvasPanel;

    private File imageFile = null;
    private double scale = 1.0;
    private double translateX;
    private double translateY;
    private int imageWidth;
    private int imageHeight;
    private int x;
    private int y;
    private int tempX;
    private int tempY;
    private Rectangle selectedRectangle = null;

    private JPanel toolsPanel;
    private Canvas canvas;

    private JButton processButton;

    public ImageCanvas(CanvasPanel canvasPanel) {
        this.canvasPanel = canvasPanel;

        toolsPanel = new JPanel();
        canvas = new Canvas();
        BoxLayout boxLayout = new BoxLayout(toolsPanel, BoxLayout.X_AXIS);
        toolsPanel.setLayout(boxLayout);
        toolsPanel.setBorder(new EmptyBorder(10,10,10,10));

        setLayout(new BorderLayout());

        setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        add(canvas);
        add(toolsPanel, BorderLayout.SOUTH);
        

        processButton = new JButton("PROCESS IMAGE");
        processButton.addActionListener((ActionEvent e) -> {
            canvasPanel.mainWindow.processImage();
        });
        
        toolsPanel.add(processButton);
    }

    public void setImage(File file) {
        canvas.setImage(file);
    }

    public File getImageFile() {
        return canvas.getImageFile();
    }

    public class Canvas extends JComponent implements MouseMotionListener, MouseListener, MouseWheelListener {

        public void Canvas() {
            addMouseMotionListener(this);
            addMouseListener(this);
            addMouseWheelListener(this);
        }

        @Override
        public void paintComponent(Graphics gr) {
            super.paintComponent(gr);
            Graphics2D g = (Graphics2D) gr;
            g.fillRect(0, 0, getWidth(), getHeight());
            g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
            g.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
            if (imageFile != null) {
                ImageIcon imageIcon = new ImageIcon(imageFile.getAbsolutePath());

                int width = imageIcon.getIconWidth();
                int height = imageIcon.getIconHeight();

                double aspectRatio = (double) width / height;

                int lWidth = getWidth();
                int lHeight = getHeight();
                if (width > lWidth) {
                    width = lWidth;
                    height = (int) (width / aspectRatio);
                }
                if (height > lHeight) {
                    height = lHeight;
                    width = (int) (height * aspectRatio);
                }

                x = (getWidth() - width) / 2;
                y = (getHeight() - height) / 2;
                imageWidth = width;
                imageHeight = height;
                g.drawImage(imageIcon.getImage(), x, y, imageWidth, imageHeight, this);

                g.scale(scale, scale);
            } else {
                int width = getWidth();
                int height = getHeight();
                setBounds(0, 0, width, height);

                int crossHeight = height / 3;
                int crossWidth = width / 3;
                int crossSize = crossHeight > crossWidth ? crossWidth : crossHeight;

                int crossThickness = crossSize / 4;

                int x = (width - crossSize) / 2;
                int y = (height - crossSize) / 2;

                Color crossColor = Color.GRAY;

                g.setColor(crossColor);
                g.fillRect(x + (crossSize - crossThickness) / 2, y, crossThickness, crossSize);
                g.fillRect(x, y + (crossSize - crossThickness) / 2, crossSize, crossThickness);

                String text = "Click to select image";
                Font font = new Font(Font.DIALOG, Font.PLAIN, 20);
                int stringWidth = getFontMetrics(font).stringWidth(text);
                int stringX = (width - stringWidth) / 2;
                int stringY = y + crossSize + 10 + font.getSize();
                g.setFont(font);
                g.drawString(text, stringX, stringY);
            }
            selectRegion(g);
        }

        private void selectRegion(Graphics2D g) {
            if (selectedRectangle != null) {
                Rectangle top = selectedRectangle;

                g.setColor(Color.WHITE);
                g.drawRect(top.x, top.y, top.width, top.height);
                g.setColor(Color.BLACK);
                g.drawRect(top.x + 1, top.y + 1, top.width, top.height);
            }
        }

        public void setImage(File file) {
            imageFile = file;
            this.setCursor(Cursor.getPredefinedCursor(Cursor.CROSSHAIR_CURSOR));
            repaint();
        }

        @Override
        public void mouseDragged(MouseEvent e) {
            int diffX = tempX - e.getX();
            int diffY = tempY - e.getY();
//        x -= diffX;
//        y -= diffY;
            selectedRectangle.width = -selectedRectangle.x + e.getX();
            selectedRectangle.height = -selectedRectangle.y + e.getY();
            tempX = e.getX();
            tempY = e.getY();
            repaint();
        }

        @Override
        public void mouseMoved(MouseEvent e) {
            translateX = e.getX();
            translateY = e.getY();
        }

        @Override
        public void mouseClicked(MouseEvent e) {
        }

        @Override
        public void mousePressed(MouseEvent e) {
            tempX = e.getX();
            tempY = e.getY();
            selectedRectangle = new Rectangle();
            selectedRectangle.x = tempX;
            selectedRectangle.y = tempY;
        }

        @Override
        public void mouseReleased(MouseEvent e) {
            selectedRectangle = null;
        }

        @Override
        public void mouseEntered(MouseEvent e) {
            if (imageFile != null) {
                this.setCursor(Cursor.getPredefinedCursor(Cursor.CROSSHAIR_CURSOR));
            } else {
                this.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            }
        }

        @Override
        public void mouseExited(MouseEvent e) {
            this.setCursor(Cursor.getDefaultCursor());
        }

        @Override
        public void mouseWheelMoved(MouseWheelEvent e) {
            double amount = e.getPreciseWheelRotation();
            scale = amount;
            repaint();
        }

        public File getImageFile() {
            return imageFile;
        }
    }
}
