package javaocr;

import java.awt.event.ActionEvent;
import java.io.File;
import javax.swing.AbstractAction;
import javax.swing.JFileChooser;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.filechooser.FileNameExtensionFilter;

/**
 *
 * @author Saurabh Maheshwari
 */
public class MMenuBar extends JMenuBar {

    private JMenu file;
    private JMenuItem openImage;
    private JMenuItem exit;
    private final MainWindow mainWindow;

    public MMenuBar(MainWindow mainWindow) {
        this.mainWindow = mainWindow;
        init();
    }

    private void init() {
        file = new JMenu("File");
        file.setMnemonic('F');

        this.add(file);

        openImage = new JMenuItem(new AbstractAction("Open Image") {

            @Override
            public void actionPerformed(ActionEvent e) {
                JFileChooser fileChooser = new JFileChooser(".");
                FileNameExtensionFilter filter = new FileNameExtensionFilter("Image Files", "jpg", "png", "gif", "jpeg");
                fileChooser.setFileFilter(filter);

                int option = fileChooser.showOpenDialog(mainWindow);
                fileChooser.setMultiSelectionEnabled(false);
                if (option == JFileChooser.APPROVE_OPTION) {
                    File selected = fileChooser.getSelectedFile();
                    mainWindow.getCanvas().setImage(selected);
                }
            }
        });

        file.add(openImage);

        exit = new JMenuItem(new AbstractAction("Exit") {

            @Override
            public void actionPerformed(ActionEvent e) {
                System.exit(0);
            }

        });
        exit.setMnemonic('x');

        file.add(exit);
    }

    public void openImage() {
        openImage.doClick();
    }
}
