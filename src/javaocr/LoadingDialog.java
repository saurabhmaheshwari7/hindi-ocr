package javaocr;

import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import javax.swing.ImageIcon;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JProgressBar;

/**
 *
 * @author Saurabh Maheshwari
 */
public class LoadingDialog extends JDialog {

    private String message;
    private final GridBagConstraints c = new GridBagConstraints();

    private JProgressBar progressBar;
    private JLabel messageLabel;

    public LoadingDialog() {
        this("", "");
    }

    public LoadingDialog(String title, String message) {
        this.message = message;
        setTitle(title);
        setSize(300, 150);
        setLayout(new GridBagLayout());
        setLocationRelativeTo(null);
        setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
        setResizable(false);

        progressBar = new JProgressBar();
        progressBar.setIndeterminate(true);
        messageLabel = new JLabel(message);

        c.ipady = 10;
        add(progressBar, c);
        c.gridy = 1;
        add(messageLabel, c);
    }

    public void setMessage(String message) {
        this.message = message;
        messageLabel.setText(message);
    }
}
