package javaocr;

import java.awt.Color;
import java.util.HashMap;

/**
 *
 * @author Saurabh Maheshwari
 */
public class Settings {

    public static final String IMAGE_CANVAS_BACKGROUND_COLOR = "image_canvas_background_color";
    public static final String DRAWING_CANVAS_BACKGROUND_COLOR = "drawing_canvas_background_color";
    public static final String OCR_LANGUAGE = "ocr_language";

    private static final HashMap<String, Object> settings;

    static {
        settings = new HashMap();
        settings.put(IMAGE_CANVAS_BACKGROUND_COLOR, Color.BLACK);
        settings.put(DRAWING_CANVAS_BACKGROUND_COLOR, Color.WHITE);
        settings.put(OCR_LANGUAGE, "hin");
    }

    public static String getString(String key) {
        return (String) get(key);
    }

    public static Color getColor(String key) {
        return (Color) get(key);
    }

    public static Object get(String key) {
        return settings.get(key);
    }
}
