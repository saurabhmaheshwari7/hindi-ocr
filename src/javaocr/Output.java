package javaocr;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

/**
 *
 * @author Saurabh Maheshwari
 */
public class Output extends JPanel {

    private final MainWindow mainWindow;

    private final JTextArea outputText;
    private final JPanel controlsPanel;

    private final GridBagConstraints c = new GridBagConstraints();

    private final JTextAreaPopupMenu textAreaPopupMenu;
    private final JButton processButton;

    public Output(MainWindow mainWindow) {
        this.mainWindow = mainWindow;

        setLayout(new BorderLayout());
        outputText = new JTextArea();
        outputText.setFont(new Font(Font.SANS_SERIF, Font.PLAIN, 20));
        controlsPanel = new JPanel(new GridBagLayout());
        controlsPanel.setPreferredSize(new Dimension(300, controlsPanel.getPreferredSize().height));

        processButton = new JButton("RECOGNIZE TEXT FROM IMAGE");
        processButton.addActionListener((ActionEvent e) -> {
            mainWindow.processImage();
        });

        textAreaPopupMenu = new JTextAreaPopupMenu(outputText);
        outputText.setComponentPopupMenu(textAreaPopupMenu);
        init();
    }

    public void setText(String text) {
        outputText.setText("");
        outputText.setText(text);
    }

    private void init() {
        add(new JScrollPane(outputText));

        controlsPanel.add(processButton);
    }
}
