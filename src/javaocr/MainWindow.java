package javaocr;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.io.File;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JSplitPane;
import net.sourceforge.tess4j.ITesseract;
import net.sourceforge.tess4j.Tesseract;
import net.sourceforge.tess4j.TesseractException;

/**
 *
 * @author Saurabh Maheshwari
 */
public class MainWindow extends JFrame {

    private static final Dimension DEFAULT_DIMENSION = new Dimension(700, 500);

    private final MMenuBar menuBar;
    private final CanvasPanel canvasPanel;
    private final Controls controls;
    private final Output output;

    private JSplitPane horizontalSplit;
    private JSplitPane verticalSplit;
    private LoadingDialog loadingDialog;

    public MainWindow() {
        setTitle("English Optical Character Recognition (OCR).");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(DEFAULT_DIMENSION);
        setLocationRelativeTo(null);

        menuBar = new MMenuBar(this);
        canvasPanel = new CanvasPanel(this);
        controls = new Controls(this);
        output = new Output(this);
        setExtendedState(JFrame.MAXIMIZED_BOTH);

        loadingDialog = new LoadingDialog();
        init();
    }

    private void init() {
        setJMenuBar(menuBar);
        int width = Toolkit.getDefaultToolkit().getScreenSize().width;
        int height = Toolkit.getDefaultToolkit().getScreenSize().height;

        horizontalSplit = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT);
        verticalSplit = new JSplitPane(JSplitPane.VERTICAL_SPLIT);
        verticalSplit.setTopComponent(canvasPanel);
        verticalSplit.setBottomComponent(output);
        verticalSplit.setContinuousLayout(true);

        verticalSplit.setDividerLocation((int) (height * 0.75));

        horizontalSplit.setLeftComponent(verticalSplit);
        horizontalSplit.setRightComponent(controls);
        horizontalSplit.setContinuousLayout(true);

        horizontalSplit.setDividerLocation((int) (width * 0.75));

        setContentPane(verticalSplit);

    }

    public boolean canProcessImage() {
        return canvasPanel.hasImage();
    }

    public void showLoadingDialog(String title, String message) {
        Thread thread = new Thread() {
            @Override
            public void run() {
                loadingDialog.setTitle(title);
                loadingDialog.setMessage(message);
                loadingDialog.setModal(true);
                loadingDialog.setVisible(true);
            }
        };
        thread.start();
    }

    public void hideLoadingDialog() {
        loadingDialog.setVisible(false);
    }

    public String processImage() {
        if (canProcessImage()) {
            Thread t = new Thread() {
                @Override
                public void run() {
                    showLoadingDialog("Loading...", "Recognizing text...");
                    File imageFile = canvasPanel.getImage();
                    ITesseract instance = new Tesseract();
                    instance.setLanguage(Settings.getString(Settings.OCR_LANGUAGE));
                    try {
                        String result = instance.doOCR(imageFile);
                        hideLoadingDialog();
                        if (result != null) {
                            output.setText(result);
                        }
                    } catch (TesseractException e) {
                        hideLoadingDialog();
                        JOptionPane.showMessageDialog(null, "An error was encountered while processing the image.", "Character Recognition Error", JOptionPane.ERROR_MESSAGE);
                    }
                }
            };
            t.start();
        } else {
            JOptionPane.showMessageDialog(null, "There is no image selected to process.", "Character Recognition Error", JOptionPane.WARNING_MESSAGE);
        }
        return null;
    }

    public MMenuBar getMMenuBar() {
        return menuBar;
    }

    public CanvasPanel getCanvas() {
        return canvasPanel;
    }
}
